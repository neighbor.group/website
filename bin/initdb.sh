#!/bin/bash

# see also: https://kvz.io/bash-best-practices.html
set -o errexit
set -o pipefail
set -o nounset

echo
echo "Dumping a database snapshot..."
echo
docker-compose exec wp mysqldump -h db -u neighbor -pneighbor neighbor > docker/initdb.d/neighbor.sql

if [ -f docker/initdb.d/neighbor.sql.gz ] ; then
  rm docker/initdb.d/neighbor.sql.gz
fi

echo "Compressing the database SQL..."
echo
gzip docker/initdb.d/neighbor.sql

echo "Done"
