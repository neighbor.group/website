(function($) {
	if ($('.map').length == 0) {
		return;
	}
	L.mapbox.accessToken = map_options.mapbox_api_key;
	var map = L.mapbox.map($('.map__base')[0]).setView([
		map_options.default_lat,
		map_options.default_lng,
	], map_options.default_zoom);
	map.addLayer(L.mapbox.tileLayer('mapbox.light'));
	L.control.locate().addTo(map);
	var hash = new L.Hash(map);

	var geospatial_layer;
	load_geospatial();
	map.on('moveend', load_geospatial);
	map.on('zoom', load_geospatial);
	map.on('resize', load_geospatial);

	function map_select(layer, feature) {
		var pod = feature.properties.pods[0];
		$('.map__selection h3').html(pod.title);
		$('.map__selection input[name="pod"]').val(pod.id);
		$('.map__selection').removeClass('hidden');
		document.getElementsByName("name")[0].focus();
		if (window.innerWidth > 768 )
			$('.map__intro').addClass('hidden');
	}


	var observer = new ResizeObserver(function() {
		if (window.innerWidth < 768 ) {
			$(".map__selection").prependTo(".map__base");
			if (! $('.map__selection').hasClass('hidden'))
				$('.map__intro').removeClass('hidden');
		} else {
			$(".map__selection").appendTo(".map__info");
			if (! $('.map__selection').hasClass('hidden'))
				$('.map__intro').addClass('hidden');
		}
	});	
	observer.observe($(".map__base")[0]);


	$('.contact').submit(function(e) {
		e.preventDefault();
		$('.contact').addClass('sending');
		var pod = $('.contact input[name="pod"]').val();
		var name = $('.contact input[name="name"]').val();
		var email = $('.contact input[name="email"]').val();
		var message = $('.contact textarea[name="message"]').val();

		$('.contact__feedback').removeClass('hidden');
		$('.contact__feedback').html('Sending message...');

		$.ajax({
			method: 'POST',
			url: $('.contact').attr('action'),
			data: {
				pod: pod,
				name: name,
				email: email,
				message: message
			},
			success: function(rsp) {
				$('.contact').removeClass('sending');
				if (rsp.ok) {
					$('.contact__feedback').html(rsp.message);
					$('.contact textarea[name="message"]').val('');
				} else if (rsp.error) {
					$('.contact__feedback').html(rsp.error);
				} else {
					$('.contact__feedback').html('Something went wrong sending your message.');
				}
			},
			error: function() {
				$('.contact').removeClass('sending');
				$('.contact__feedback').html('Something went wrong sending your message.');
			}
		});
	});

	function load_geospatial() {
		var bounds = map.getBounds();
		$.ajax({
			method: 'GET',
			url: '/wp-json/geospatial/bbox',
			data: {
				lng_min: bounds._southWest.lng,
				lat_min: bounds._southWest.lat,
				lng_max: bounds._northEast.lng,
				lat_max: bounds._northEast.lat
			},
			success: function(rsp) {
				if (geospatial_layer) {
					geospatial_layer.remove();
				}
				geospatial_layer = L.geoJson(rsp.data, {
					style: function (feature) {
						var color = 'rgba(0, 0, 0, 0.25)';
						if (feature.properties.pods &&
						    feature.properties.pods.length > 0) {
							color = '#cd2653';
						}
						return {
							color: color,
							weight: 1,
							opacity: 0.25
						};
					},
					onEachFeature: function(feature, layer) {
						layer.on('mouseover', function() {
							layer.setStyle({
								opacity: 1
							});
						});
						layer.on('mouseout', function() {
							layer.setStyle({
								opacity: 0.25
							});
						});
						layer.on('click', function() {
							if (feature.properties.pods &&
							    feature.properties.pods.length > 0) {
								map_select(layer, feature);
							} else {
								$('.map__selection').addClass('hidden');
								$('.map__intro').removeClass('hidden');
								$('.map__geoid').html('Census block group ' + feature.id);
							}
						});
					}
				}).addTo(map);
			}
		});
	}
})(jQuery);
