#!/bin/bash

# see also: https://kvz.io/bash-best-practices.html
set -o errexit
set -o pipefail
set -o nounset

echo
echo "Let's get this project set up."
echo

# Begin the installation

echo "Setting up .env ..."
echo
cp .env.example .env

echo "What is your ACF Pro key?"
read -p "> " ACF_PRO_KEY
echo >> .env
echo "# This is for the ACF Pro installer plugin" >> .env
echo "# See: https://github.com/schliflo/acf-pro-installer" >> .env
echo "ACF_PRO_KEY=$ACF_PRO_KEY" >> .env
echo
echo "Thank you!"

echo "Bringing up project containers..."
echo
docker-compose up --build --no-start
echo

echo "Installing PHP dependencies and WordPress plugins..."
echo
docker-compose run --rm wp composer install
echo

echo "Shutting down containers..."
echo
docker-compose stop
echo

echo "-------------------------------"
echo "Install completed successfully."
echo "-------------------------------"
echo
echo "Run './bin/start.sh' and get to work."
echo
