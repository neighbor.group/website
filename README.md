# neighbor.group

Software for organizing local mutual aid groups.

## Requirements
### Linux
- [Docker CLI](https://docs.docker.com/engine/install/ubuntu/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Windows or Mac
- [Docker Desktop](https://www.docker.com/products/docker-desktop)

# First Time Setup
## Quick Start

Clone this repo
> With SSH
```bash
git clone git@code.phiffer.org:neighbor.group/website.git
```

> With HTTPS
```bash
git clone https://code.phiffer.org/neighbor.group/website.git
```

Run the one-time installation script
```bash
cd website/
./bin/startup.sh
```

Start hacking ...
> **Note:**<br />
>- The installation script [ `startup.sh` ](bin/startup.sh) is written to install the project on your local machine or on [Play With Docker](https://labs.play-with-docker.com/)<br />
>- After successful run of the installation script [ `startup.sh` ](bin/startup.sh), the containers can be stopped by running `docker-compose down` and restarted by running `docker-compose up [-d]`<br />
>- The source code files of the custom the theme are located at [`./wp/wp-content/themes/neighbor.group`](wp/wp-content/themes/neighbor.group)

# Appendix
## Setting up Geospatial index

This has a [parallel repo](https://code.phiffer.org/neighbor.group/geospatial/) with geospatial data and scripts.

```bash
cd ..
git clone git@code.phiffer.org:neighbor.group/geospatial.git
cp -r geospatial/data website/wp/wp-content/uploads/
cd website
docker-compose exec wp wp geospatial ../../uploads/data/
```

The startup script ([ `startup.sh` ](bin/startup.sh)) downloads the geospatial data to this project from the [parallel repo](https://code.phiffer.org/neighbor.group/geospatial/) on [line 156](https://code.phiffer.org/neighbor.group/website/blob/main/bin/startup.sh#L156)