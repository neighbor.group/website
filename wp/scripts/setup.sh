#!/bin/bash

# see also: https://kvz.io/bash-best-practices.html
set -o errexit
set -o pipefail
set -o nounset

# Configure nginx
sed -i 's#try_files $uri $uri/ =404;#try_files $uri $uri/ /index.php$is_args$args;#' /etc/nginx/sites-available/default.conf

# Configure WordPress
if [ ! -f /var/www/html/wp-config.php ]; then
	echo "Configuring WordPress..."
	wp --path=/var/www/html config create \
		--dbname=$WORDPRESS_DB_NAME \
		--dbuser=$WORDPRESS_DB_USER \
		--dbpass=$WORDPRESS_DB_PASSWORD \
		--dbhost=$WORDPRESS_DB_HOST \
		--extra-php <<PHP
// This allows the WordPress site to run securly behind the ups-dock reverse proxy
if ( strpos( \$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https' ) !== false ) {
	\$_SERVER['HTTPS'] = 'on';
}
// Check for a local config to override defaults
if ( file_exists( __DIR__ . '/conf/wp-config-local.php' ) ) {
	require_once __DIR__ . '/conf/wp-config-local.php';
}
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );
PHP
fi
