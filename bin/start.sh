#!/bin/bash

# see also: https://kvz.io/bash-best-practices.html
set -o errexit
set -o pipefail
set -o nounset

stop_containers() {
  echo "Shutting off containers..."
  echo
  docker-compose stop
  echo
  echo "All done!"
}

trap stop_containers SIGINT

echo "Starting containers for WordPress environment..."
echo
docker-compose up -d
echo

echo "Running composer install ..."
echo
docker-compose exec wp composer install
echo
