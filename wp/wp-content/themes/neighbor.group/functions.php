<?php

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

function ng_register_post_types() {
	$labels = array(
		'name'          => __( 'Pods' ),
		'singular_name' => __( 'Pod' ),
		'add_new_item'  => __( 'Add New Pod' ),
	);
	register_post_type(
		'pod',
		array(
			'labels'        => $labels,
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 7,
			'menu_icon'     => 'dashicons-groups',
			'rewrite'       => array( 'slug' => 'pods' ),
			'supports'      => array( 'title' ),
			'taxonomies'    => array(),
		)
	);
	$labels = array(
		'name'          => __( 'People' ),
		'singular_name' => __( 'Person' ),
		'add_new_item'  => __( 'Add New Person' ),
	);
	register_post_type(
		'person',
		array(
			'labels'        => $labels,
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 5,
			'menu_icon'     => 'dashicons-admin-users',
			'rewrite'       => array( 'slug' => 'people' ),
			'supports'      => array( 'title', 'thumbnail' ),
			'taxonomies'    => array(),
		)
	);
}
add_action('init', 'ng_register_post_types');

function ng_register_blocks() {
	acf_register_block_type(array(
		'name'            => 'map',
		'title'           => 'Map',
		'description'     => 'An interactive map block.',
		'category'        => 'common',
		'icon'            => 'location',
		'mode'            => 'preview',
		'render_callback' => 'ng_map_html'
	));
}
add_action('acf/init', 'ng_register_blocks');

function ng_script($path, $deps = array(), $in_footer = false) {
	$base_url = get_stylesheet_directory_uri();
	$base_dir = __DIR__;

	$handle = basename($path);
	$src = "$base_url/$path";
	$version = filemtime("$base_dir/$path");

	wp_enqueue_script($handle, $src, $deps, $version, $in_footer);
	return $handle;
}

function ng_style($path, $deps = array(), $media = 'all') {
	$base_url = get_stylesheet_directory_uri();
	$base_dir = __DIR__;

	$handle = basename($path);
	$src = "$base_url/$path";
	$version = filemtime("$base_dir/$path");

	wp_enqueue_style($handle, $src, $deps, $version, $media);
	return $handle;
}

function ng_enqueue_scripts() {
	$mapbox_js       = ng_script("node_modules/mapbox.js/dist/mapbox.js");
	$mapbox_css      = ng_style("node_modules/mapbox.js/dist/mapbox.css");
	$fontawesome_css = ng_style("node_modules/@fortawesome/fontawesome-free/css/all.min.css");
	$hash_js         = ng_script("node_modules/leaflet-hash/leaflet-hash.js");
	$locate_js       = ng_script("node_modules/leaflet.locatecontrol/dist/L.Control.Locate.min.js");
	$locate_css      = ng_style("node_modules/leaflet.locatecontrol/dist/L.Control.Locate.min.css");
	$map_deps        = array('jquery', $mapbox_js, $locate_js, $hash_js);
	$map_js          = ng_script("js/map.js", $map_deps, true);

	wp_localize_script($map_js, 'map_options', array(
		'default_lat'    => get_field('map_default_lat', 'options'),
		'default_lng'    => get_field('map_default_lng', 'options'),
		'default_zoom'   => get_field('map_default_zoom', 'options'),
		'mapbox_api_key' => get_field('mapbox_api_key', 'options')
	));
}
add_action('wp_enqueue_scripts', 'ng_enqueue_scripts');

function ng_pod_locations() {

	$locations = array();

	$pods = get_posts(array(
		'post_type' => 'pod',
		'posts_per_page' => -1
	));
	foreach ($pods as $pod) {
		$locations[] = array(
			'name' => $pod->post_title,
			'link' => get_permalink($pod->ID),
			'lat' => get_field('latitude', $pod->ID),
			'lon' => get_field('longitude', $pod->ID)
		);
	}

	return json_encode($locations);
}

function ng_geospatial_index($args) {
	global $wpdb, $table_prefix;

	if (empty($args[0])) {
		echo "Usage: wp geospatial [path]\n";
		exit(1);
	}

	$table_name = "{$table_prefix}geospatial";

	if (in_array('reset', $args)) {
		echo "Dropping table $table_name\n";
		$wpdb->query("DROP TABLE IF EXISTS $table_name");
	}

	try {
		$wpdb->query("SELECT 1 FROM $table_name LIMIT 1");
	} catch(Exception $err) {
		echo "Creating table '$table_name'\n";
		$wpdb->query("
			CREATE TABLE IF NOT EXISTS $table_name (
				geoid VARCHAR(255),
				name VARCHAR(255),
				lng_min FLOAT,
				lat_min FLOAT,
				lng_max FLOAT,
				lat_max FLOAT,
				boundary TEXT,
				boundary_geom GEOMETRY
			)
		");
		$wpdb->query("
			CREATE INDEX {$table_name}_gix ON $table_name USING GIST (boundary_geom)
		");
	}

	$files = glob("{$args[0]}/*.geojson");

	$wpdb->query("START TRANSACTION");

	foreach ($files as $file) {
		echo "$file\n";
		$geojson = file_get_contents($file);
		$feature = json_decode($geojson, 'as hash');
		$sql = $wpdb->prepare("
			INSERT INTO $table_name (
					geoid,
					name,
					lng_min,
					lat_min,
					lng_max,
					lat_max,
					boundary
				) VALUES (%s, %s, %f, %f, %f, %f, %s)
			",
			$feature['id'],
			$feature['properties']['name'],
			$feature['bbox'][0],
			$feature['bbox'][1],
			$feature['bbox'][2],
			$feature['bbox'][3],
			json_encode($feature['geometry'])
		);
		$result = $wpdb->query($sql);
		if ($result === false) {
			echo "$wpdb->last_error\nSQL:\n$sql\n";
			break;
		}
	}

	$wpdb->query("COMMIT");
	echo "Indexing geometry\n";
	$wpdb->query("
		UPDATE $table_name
		SET boundary_geom = ST_GeomFromGeoJSON(boundary)
	");

}
if (class_exists('WP_CLI')) {
	WP_CLI::add_command( 'geospatial', 'ng_geospatial_index' );
}

function ng_rest_api_init() {
	register_rest_route('geospatial', 'bbox', array(
		'methods' => 'GET',
		'callback' => 'ng_api_geospatial_bbox'
	));
	register_rest_route('pods', 'contact', array(
		'methods' => 'POST',
		'callback' => 'ng_api_pods_contact'
	));
}
add_action('rest_api_init', 'ng_rest_api_init');

function ng_api_geospatial_bbox($req) {

	if (empty($req['lng_min']) || empty($req['lat_min']) ||
	    empty($req['lng_max']) || empty($req['lat_max'])) {
		return array(
			'ok' => false,
			'error' => "Error: include 'lng_min', 'lat_min', 'lng_max', 'lat_max'."
		);
	}

	global $wpdb, $table_prefix;

	$values = array(
		$req['lng_min'], $req['lat_min'],
		$req['lng_max'], $req['lat_min'],
		$req['lng_max'], $req['lat_max'],
		$req['lng_min'], $req['lat_max'],
		$req['lng_min'], $req['lat_min']
	);

	$sql = $wpdb->prepare("
		SELECT geoid AS id, boundary AS geometry, name
		FROM {$table_prefix}geospatial
		WHERE Intersects(GeomFromText('POLYGON((%f %f,%f %f,%f %f,%f %f,%f %f))', 4326), boundary_geom)
	", ...$values);

	$results = $wpdb->get_results($sql, 'ARRAY_A');
	$geoids = array_map(function($feature) {
		return $feature['id'];
	}, $results);
	$pods = ng_get_pods_by_geoids($geoids);

	foreach ($results as $index => $feature) {
		$results[$index]['type'] = 'Feature';
		$results[$index]['geometry'] = json_decode($feature['geometry'], true);
		$geoid = $feature['id'];
		$feature_pods = array();
		if (! empty($pods[$geoid])) {
			$feature_pods = $pods[$geoid];
		}
		$results[$index]['properties'] = array(
			'geoid' => $geoid,
			'name' => $feature['name'],
			'pods' => $feature_pods
		);
		unset($results[$index]['name']);
	}

	return array(
		'ok' => true,
		'data' => array(
			'type' => 'FeatureCollection',
			'features' => $results
		)
	);
}

function ng_get_pods_by_geoids($geoids) {
	global $wpdb;
	$pod_lookup = array();
	$pods = array();
	$results = $wpdb->get_results("
		SELECT post_id, meta_value
		FROM $wpdb->postmeta
		WHERE meta_key = 'block_groups'
	");
	foreach ($results as $result) {
		$pod_geoids = explode(',', $result->meta_value);
		foreach ($pod_geoids as $geoid) {
			$geoid = trim($geoid);
			if (in_array($geoid, $geoids)) {
				if (empty($pods[$geoid])) {
					$pods[$geoid] = array();
				}
				if (empty($pod_lookup[$result->post_id])) {
					$pod = get_post($result->post_id);
					$pod_lookup[$result->post_id] = array(
						'id'    => $pod->ID,
						'title' => $pod->post_title,
						'link'  => get_permalink($pod->ID)
					);
				}
				array_push($pods[$geoid], $pod_lookup[$result->post_id]);
			}
		}
	}
	return $pods;
}

function ng_map_html() {
	?>
	<div class="map">
		<div class="map__info">
			<div class="map__intro">
				<h3>Mutual aid pod map</h3>
				<ul>
					<li>Click on the <i class="fa fa-map-marker"></i> button to zoom into where you live.</li>
					<li>Select an area by clicking on it.</li>
					<li>The areas shaded in red have existing pods.</li>
					<li>Don’t see a pod where you live? <a rel="noopener noreferrer" href="https://docs.google.com/document/d/1rSGsImDwVc4VsbZFgqs4IMYABFecz0QKNyhcnpQS3pE/edit?usp=sharing" target="_blank">Start a new one!</a></li>
				</ul>
				<div class="map__geoid"></div>
			</div>
			<div class="map__selection hidden">
				<h3></h3>
				<p><i class="fa fa-envelope"></i> Send a message to the pod's contact.</p>
				<form class="contact" action="/wp-json/pods/contact" method="post">
					<input type="hidden" name="pod" value="">
					<label>
						Your name
						<input type="text" name="name">
					</label>
					<label>
						Your email address
						<input type="email" name="email">
					</label>
					<label>
						Your message
						<textarea name="message" cols="50" rows="5"></textarea>
					</label>
					<div class="contact__feedback hidden"></div>
					<input type="submit" value="Send message" class="btn">
				</form>
			</div>
		</div>
		<div class="map__base"></div>
	</div>
	<?php
}

function ng_api_pods_contact($req) {
	if (empty($req['pod']) ||
	    empty($req['name']) ||
	    empty($req['email']) ||
	    empty($req['message'])) {
		return array(
			'ok' => false,
			'error' => 'Please fill in each field before sending.'
		);
	}

	$pod = get_post($req['pod']);
	if (empty($pod) || $pod->post_type != 'pod') {
		return array(
			'ok' => false,
			'error' => 'Could not find the pod you are trying to contact.'
		);
	}

	$point_person = get_field('point_person', $pod->ID);
	if (empty($point_person)) {
		$email_to = get_option('admin_email');
	} else {
		$email_to = get_field('email', $point_person);
	}

	$subject = "Message from {$req['name']}";
	$message = $req['message'];
	$headers = array(
		"Reply-To: {$req['email']}"
	);
	wp_mail($email_to, $subject, $message, $headers);

	return array(
		'ok' => true,
		'message' => 'Your message has been sent.'
	);
}
